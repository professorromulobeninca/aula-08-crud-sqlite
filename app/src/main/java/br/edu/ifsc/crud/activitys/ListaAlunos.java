package br.edu.ifsc.crud.activitys;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import br.edu.ifsc.crud.R;
import br.edu.ifsc.crud.models.Aluno;
import br.edu.ifsc.crud.adapters.AlunoAdapter;

public class ListaAlunos extends AppCompatActivity {
    ListView lv ;
    SQLiteDatabase bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos);

        lv = findViewById(R.id.listView);
        bd = openOrCreateDatabase("alunos" , MODE_PRIVATE, null);
        montaListagem();
    }

    public  void montaListagem(){
        Cursor registros = bd.query("alunos", null , null , null, null , null , null);
        registros.moveToFirst();
      //*  Trecho de código utilizado para  Listar apenas uma informação por item */

        ArrayList<Aluno> itens = new ArrayList<>();

        do {
            int codigo = registros.getInt(registros.getColumnIndex("id"));
            String nome_uc = registros.getString(registros.getColumnIndex("nome_UC"));
            Double nota = registros.getDouble(registros.getColumnIndex("nota"));
            itens.add (new Aluno(codigo, nome_uc, nota));
        } while (registros.moveToNext());

        AlunoAdapter adapter = new AlunoAdapter(this, R.layout.template_lista_aluno,itens);

        /** Trecho de código utilizado para  Listar apenas uma informação por item */

//          ArrayList<String> itens = new ArrayList<>();
//        do {
//           String s= new String();
//           s= registros.getString(registros.getColumnIndex("nome_UC"));
//           itens.add(s);
//        } while (registros.moveToNext());
//
//        ArrayAdapter<String> adapter =new ArrayAdapter<String>(
//                this,
//                android.R.layout.simple_list_item_1,
//                android.R.id.text1,
//                itens
//                )    ;
        lv.setAdapter(adapter);
    }
}
